const express = require("express");
const mongoose = require("mongoose");
const CustomerRouter = require("./app/router/routerCustomer");
const OrderRouter = require("./app/router/routerOrder");
const ProductTypeRouter = require("./app/router/routerPorductType");
const ProductRouter = require("./app/router/routerProduct");
//Khỏi tạo app  nodeJS
const app = new express();

//Khai báo middleware json
app.use(express.json());

//Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))
//  Khai báo cổng chạy nodejs
const port = 8000;

mongoose.connect("mongodb://localhost:27017/CRUD_Shop24h", (err) => {
    if (err) {
        throw err;
    }

    console.log("kết nối thành công!");
})
// sử dung router
app.use('/', ProductTypeRouter);
app.use('/', ProductRouter); 
app.use('/', CustomerRouter);
app.use('/', OrderRouter)

// Khai báo chạy trên cổng nodeJS
app.listen(port, () => {
    console.log(`App chạy trên cổng ${port}`);
})
