//bước 1: khai báo mongose
const mongoose = require("mongoose")
// bước 3: khai báo thư viện schema
const Schema = mongoose.Schema
// khởi tạo các thuộc tính
const OrderSchema = {
    _id: {
        type: mongoose.Types.ObjectId
    },
    orderDate: {
        type: Date,
        default: Date.now()
    },
    shippedDate: {
        type: String
    },
    note   : {
        type: String
    },

    orderDetail: [{}],
    cost: {
        type: Number,
        default: 0
     },
    timeCreated: {
        type: Date,
        default: Date.now()
    },
    timeUpdated: {
        type: Date,
        default: Date.now()
    }

}
module.exports = mongoose.model("order", OrderSchema);