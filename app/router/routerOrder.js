//khai báo thư viện express
const express = require('express');
const { getAllOrder, createOrder, updateOrderById, getOrderById, deleteOrderId, createOrderOfCustomer } = require('../controller/controllerOrder');

const { ProductTypeURLMiddleware } = require('../middlewares/courseMiddleware');


// roter
const OrderRouter = express.Router();
// CRUD
// create customer of ỏder
OrderRouter.post("/order/:CustomerId/orders", ProductTypeURLMiddleware, createOrderOfCustomer);
// get all Order
OrderRouter.get("/order", ProductTypeURLMiddleware, getAllOrder);
// tao  Order
OrderRouter.post("/order", ProductTypeURLMiddleware, createOrder);
// update Order by id
OrderRouter.put("/order/:OrderId", ProductTypeURLMiddleware, updateOrderById);
// get by id Order
OrderRouter.get("/order/:OrderId", ProductTypeURLMiddleware, getOrderById);
// delete by id Order
OrderRouter.delete("/order/:OrderId", ProductTypeURLMiddleware, deleteOrderId);
//ecports
module.exports = OrderRouter;

