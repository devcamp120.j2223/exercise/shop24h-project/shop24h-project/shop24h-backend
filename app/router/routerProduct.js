//khai báo thư viện express
const express = require('express');
const { getAllProduct, createProduct, updatteProductById, deleteProductId, getProductById } = require('../controller/controllerProduct');
const { ProductTypeURLMiddleware } = require('../middlewares/courseMiddleware');


// roter
const ProductRouter = express.Router();
// CRUD
// get all producttpe
ProductRouter.get("/product", ProductTypeURLMiddleware, getAllProduct);
// tao  producttype
ProductRouter.post("/product", ProductTypeURLMiddleware, createProduct);
// update producttype by id
ProductRouter.put("/product/:ProductId", ProductTypeURLMiddleware, updatteProductById);
// get by id producttype
ProductRouter.get("/product/:ProductId", ProductTypeURLMiddleware, getProductById);
// delete by id productype
ProductRouter.delete("/product/:ProductId", ProductTypeURLMiddleware, deleteProductId);
//ecports
module.exports = ProductRouter;

